//
//  SearchType.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 04/11/2020.
//

import SwiftUI

public struct SearchType: Equatable {
    public let text: String
    public let value: Int
}
