//
//  StateManager.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 29/10/2020.
//

import SwiftUI

class StateManager: ObservableObject {
    @Published var showToast: Bool = false
    @Published var showMenu: Bool = false
    @Published var showSheet: Bool = false
    
    @Published var sheetMode: String = "scan"
    @Published var toastText: String = ""
    @Published var searchString: String = ""
    @Published var searchType: Int = 0
    
    func setShowToast(text: String) {
        self.toastText = text
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.showToast.toggle()
        }
    }
    
    // Function that toggles side menu
    func setShowMenu() {
        self.showMenu.toggle()
    }
    
    // Function that triggers application sheet view
    func setShowSheetState (mode: String) {
        self.sheetMode = mode
        self.showSheet.toggle()
    }
}
