//
//  MenuContent.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 29/10/2020.
//

import SwiftUI

struct SideMenuContent: View {
    @EnvironmentObject var state: StateManager

    var options = [SearchType(text: "Text contains", value: 0), SearchType(text: "Exact match", value: 1)]
    var body: some View {
        NavigationView {
            Form {
                Section() {
                    TextField("copied", text: $state.searchString)
                    Picker(selection: $state.searchType, label: Text("Search type")) {
                        ForEach(0 ..< options.count) {
                            Text(self.options[$0].text)
                        }
                    }
                }
            }.navigationBarTitle(Text("SwiftUI"))
        }
    }
}
