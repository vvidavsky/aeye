//
//  SideMenuMech.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 29/10/2020.
//

import SwiftUI

struct SideMenuDrawer: View {
    @EnvironmentObject var state: StateManager
    private let width = UIScreen.main.bounds.width - 100
    
    var body: some View {
        GeometryReader { geo in
        ZStack {
            SideMenuContent()
                .environmentObject(state)
                .frame(width: self.width, height: geo.size.height - 61)
                .offset(x: state.showMenu ? 0 : -self.width)
                .animation(.default)
            Spacer()
        }
        }
    }
}
