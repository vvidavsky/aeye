//
//  AppToolBar.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 28/10/2020.
//

import SwiftUI
import MobileCoreServices
import Flurry_iOS_SDK

struct ToolbarView: View {
    @Binding var scannedText: String
    @EnvironmentObject var state: StateManager
    
    var interstitial:Interstitial = Interstitial()
    
    var body: some View {
        HStack(spacing: 20) {
//            Button(action: {
//                state.setShowMenu()
//                self.hideKeyboard()
//            }) {
//                Image(systemName: "ellipsis").toolbarButton()
//            }
            Spacer()
            Button(action: {
                // Copying text to clipboard
                UIPasteboard.general.setValue(scannedText, forPasteboardType: kUTTypePlainText as String)
                state.setShowToast(text: Translator.translate(text: "copied"))
                self.hideKeyboard()
                Flurry.logEvent("Text copied to clipboard")
            }) {
                Image(systemName: "doc.on.doc").toolbarButton()
            }.disabled(scannedText.count == 0)
            
            Button(action: {
                self.scannedText = ""
                self.hideKeyboard()
                Flurry.logEvent("Text deleted from stage")
                //self.interstitial.showAd()
            }) {
                Image(systemName: "trash").toolbarButton()
            }.disabled(scannedText.count == 0)
            
            Button(action: {
                state.setShowSheetState(mode: "share")
                self.hideKeyboard()
                Flurry.logEvent("Text shared")
            }) {
                Image(systemName: "square.and.arrow.up").toolbarButton()
            }.disabled(scannedText.count == 0)
        }.padding(EdgeInsets(top: 10, leading: 15, bottom: 10, trailing: 15))
        .border(width: 1, edges: [.top], color: .blue)
        .onTapGesture {
            self.hideKeyboard()
        }
    }
}
