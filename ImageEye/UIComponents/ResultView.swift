//
//  ResultView.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 28/10/2020.
//

import SwiftUI

struct ResultView: View {
    @Binding var recognizedText: String

    var body: some View {
        GeometryReader { geo in
            if (recognizedText.count > 0) {
                VStack {
                    TextEditor(text: $recognizedText).padding(10)
                    //TextWithAttributedString(attributedString: generateAttributedString(with: "swift", targetString: recognizedText)!)
                }.frame(width: geo.size.width, height: geo.size.height)
            } else {
                EmptyContent()
            }
        }
    }
}

