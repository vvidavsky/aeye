//
//  MainView.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 28/10/2020.
//

import SwiftUI
import ExytePopupView

struct ImageScanner: View {
    @State private var launching = true
    @State private var recognizedText = ""
    @State var isDrawerOpen: Bool = false
    @ObservedObject var state = StateManager()
    
    var body: some View {
        ZStack {
            if launching {
                LaunchScreen()
            } else {
                VStack {
                    Banner()
                    ResultView(recognizedText: $recognizedText)
                    ScanButton().environmentObject(state)
                    ToolbarView(scannedText: $recognizedText).environmentObject(state)
                }.padding(EdgeInsets(top: 5, leading: 0, bottom: 0, trailing: 0))
                .onTapGesture {
                    if self.state.showMenu {
                        self.state.setShowMenu()
                    }
                }
                .sheet(isPresented: $state.showSheet) {
                    SheetsHub(recognizedText: $recognizedText).environmentObject(state)
                }
            }
            SideMenuDrawer().environmentObject(state)
        }.popup(isPresented: $state.showToast, type: .floater(), position: .bottom, autohideIn: 3) {
            ToastTemplate().environmentObject(state)
        }.onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                withAnimation {
                    self.launching = false
                }
            }
        }
    }
}
