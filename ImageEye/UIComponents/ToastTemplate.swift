//
//  ToastTemplate.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 29/10/2020.
//

import SwiftUI

struct ToastTemplate: View {
    @EnvironmentObject var state: StateManager
    
    var body: some View {
        HStack {
            Text(state.toastText)
        }
        .padding((EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20)))
        .background(Color.purple)
        .foregroundColor(Color.white)
//        .background(Color(red: 0.85, green: 0.8, blue: 0.95))
        .cornerRadius(15.0)
    }
}

