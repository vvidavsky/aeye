//
//  EmptyContentPlaceholder.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 30/10/2020.
//

import SwiftUI

struct EmptyContent: View {
    private let opac = 0.3
    var body: some View {
        HStack {
            Spacer()
            VStack {
                Spacer()
                Image(systemName: "camera.viewfinder").font(.system(size: 120.0)).opacity(opac)
                Divider()
                Text(Translator.translate(text: "empty_ph")).opacity(opac)
                Spacer()
            }
            Spacer()
        }
    }
}
