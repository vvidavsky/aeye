//
//  CameraScanner.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 28/10/2020.
//

import SwiftUI
import VisionKit
import Vision


struct ScanDocumentView: UIViewControllerRepresentable {
    typealias UIViewControllerType = VNDocumentCameraViewController
    @Environment(\.presentationMode) var presentationMode
    
    @Binding var recognizedText: String
    
    func makeUIViewController(context: Context) -> VNDocumentCameraViewController {
        let documentViewController = VNDocumentCameraViewController()
        documentViewController.delegate = context.coordinator
        return documentViewController
    }
    
    func updateUIViewController(_ uiViewController: VNDocumentCameraViewController, context: Context) {
                // nothing to do here
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(recognizedText: $recognizedText, parent: self)
    }
    
}
