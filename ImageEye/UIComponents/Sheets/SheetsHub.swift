//
//  SheetsHub.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 01/11/2020.
//

import SwiftUI

struct SheetsHub: View {
    @Binding var recognizedText: String
    @EnvironmentObject var state: StateManager
    var body: some View {
        if (state.sheetMode == "scan") {
            ScanDocumentView(recognizedText: $recognizedText)
        } else if (state.sheetMode == "share") {
            ShareSheet(activityItems: [recognizedText])
        }
    }
}
