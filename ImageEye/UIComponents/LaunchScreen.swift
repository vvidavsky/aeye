//
//  LaunchScreen.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 13/11/2020.
//

import SwiftUI

struct LaunchScreen: View {
    var body: some View {
        GeometryReader { geo in
            VStack {
                Image("Aeye_logo").resizable().frame(width: 250, height: 250, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            }
            .frame(width: geo.size.width, height: geo.size.height)
            .background(LinearGradient(gradient: Gradient(colors: [Color(hex: 0xd004ee), Color(hex: 0x8b019b)]), startPoint: .leading, endPoint: .trailing))
        }
    }
}
