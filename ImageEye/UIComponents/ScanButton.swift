//
//  ScanButton.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 28/10/2020.
//

import SwiftUI
import Flurry_iOS_SDK


struct ScanButton: View {
    @EnvironmentObject var state: StateManager
    var body: some View {
        Button(action: {
            state.setShowSheetState(mode: "scan")
            Flurry.logEvent("Scan button pressed")
        }) {
            Image(systemName: "camera")
            Text(Translator.translate(text: "start_scan"))
        }
        .padding()
        .foregroundColor(.white)
        .background(Capsule().fill(Color.blue))
    }
}
