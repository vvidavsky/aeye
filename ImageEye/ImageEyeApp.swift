//
//  ImageEyeApp.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 28/10/2020.
//

import SwiftUI
import GoogleMobileAds
import Flurry_iOS_SDK

@main
struct ImageEyeApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["dd3d06759f06253758da9a2229dec432"]
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        Flurry.startSession("DWJQNQMHY29BWFRYXQ9Z", with: FlurrySessionBuilder
              .init()
              .withCrashReporting(true)
              .withLogLevel(FlurryLogLevelAll))
        return true
    }
}
