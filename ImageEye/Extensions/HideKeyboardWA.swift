//
//  HideKeyboardWA.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 30/10/2020.
//

import SwiftUI

#if canImport(UIKit)
extension View {
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
#endif
