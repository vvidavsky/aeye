//
//  StringExtension.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 29/10/2020.
//

import SwiftUI

class Translator {
    static func translate(text: String) -> String{
        return String.localizedStringWithFormat(NSLocalizedString(text, comment: ""))
    }
}
