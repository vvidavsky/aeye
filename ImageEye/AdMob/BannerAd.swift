//
//  BannerAd.swift
//  ImageEye
//
//  Created by Vlad Vidavsky on 01/11/2020.
//

import SwiftUI
import GoogleMobileAds
//import UIKit

struct BannerVC: UIViewRepresentable  {
    
    func makeUIView(context: UIViewRepresentableContext<BannerVC>) -> GADBannerView {
        let view = GADBannerView(adSize: kGADAdSizeBanner)
        #if DEBUG
        view.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        #else
        view.adUnitID = "ca-app-pub-2869104140578710/5079161625"
        #endif
        view.rootViewController = UIApplication.shared.windows.first?.rootViewController
        view.load(GADRequest())
        return view
    }
    
    
    func updateUIView(_ uiView: GADBannerView, context: UIViewRepresentableContext<BannerVC>) {
        
    }
}

struct Banner: View {
    var body: some View {
        HStack{
            BannerVC().frame(width: 360, height: 50, alignment: .center)
        }
    }
}
